package project.test.com.testproject.model;


import javax.inject.Inject;

import retrofit2.Retrofit;

public class ApiClient {

    private ApiEndpoint apiEndpoint;

    @Inject
    ApiClient(Retrofit retrofit){
        apiEndpoint = retrofit.create(ApiEndpoint.class);
    }

    public ApiEndpoint getApiEndpoint(){
        return apiEndpoint;
    }
}
