package project.test.com.testproject.model;


import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Board {
    @SerializedName("is_delete")
    private boolean is_delete;

    @SerializedName("name")
    private String name;

    public String getName() {
        return name;
    }

    public boolean isDelete(){
        return is_delete;
    }

}
