package project.test.com.testproject.di;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import project.test.com.testproject.App;
import project.test.com.testproject.view.MainActivity;

@Component(modules = {AppModule.class, NetworkModule.class})
@Singleton
public interface AppComponent {

    void inject(MainActivity activity);

    void inject(App app);

    @Component.Builder
    interface Builder {
        @BindsInstance
        Builder context(App application);
        AppComponent build();
    }
}
