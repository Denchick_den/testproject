package project.test.com.testproject.view;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;

import java.util.List;

import javax.inject.Inject;

import project.test.com.testproject.App;
import project.test.com.testproject.R;
import project.test.com.testproject.databinding.ContentMainBinding;
import project.test.com.testproject.model.Board;

public class MainActivity extends AppCompatActivity implements MainActivityView {

    private BoardListAdapter boardListAdapter;
    private ContentMainBinding contentMainBinding;

    @Inject MainActivityPresenter mainActivityPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        App.getInstance(this).getAppComponent().inject(this);
        super.onCreate(savedInstanceState);
        getLifecycle().addObserver(mainActivityPresenter);

        boardListAdapter = new BoardListAdapter();

        contentMainBinding = DataBindingUtil.setContentView(this, R.layout.content_main);
        setSupportActionBar(contentMainBinding.toolbar);
        contentMainBinding.floatingActionButton.setOnClickListener(view -> mainActivityPresenter.sendData());
        contentMainBinding.recyclerView.setLayoutManager(new LinearLayoutManager(this));
        contentMainBinding.recyclerView.setAdapter(boardListAdapter);

        mainActivityPresenter.attachView(this);
        mainActivityPresenter.loadData();
    }

    @Override
    public void setData(List<Board> boards) {
        boardListAdapter.setBoards(boards);
    }

    @Override
    public void addItem(Board board) {
        boardListAdapter.setItem(board);
        Snackbar.make(contentMainBinding.root, getString(R.string.item_added), 2000).show();
    }

    @Override
    public void onError(Throwable error) {
        Snackbar.make(contentMainBinding.root, error.getMessage(), 2000).show();
    }
}
