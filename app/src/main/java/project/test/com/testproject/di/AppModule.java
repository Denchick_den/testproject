package project.test.com.testproject.di;


import android.content.Context;

import javax.inject.Singleton;

import dagger.Binds;
import dagger.Module;
import project.test.com.testproject.App;

@Module
public abstract class AppModule {
    @Binds
    @Singleton
    public abstract Context context(App app);
}
