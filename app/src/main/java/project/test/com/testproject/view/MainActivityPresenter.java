package project.test.com.testproject.view;


import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.LifecycleObserver;
import android.arch.lifecycle.OnLifecycleEvent;

import com.google.gson.JsonObject;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import project.test.com.testproject.model.ApiClient;

class MainActivityPresenter implements LifecycleObserver {

    private final ApiClient apiClient;
    private MainActivityView view;

    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    @Inject
    MainActivityPresenter(ApiClient apiClient) {
        this.apiClient = apiClient;
    }

    void attachView(MainActivityView view) {
        this.view = view;
    }

    void loadData() {
        disposeOnFinish(apiClient.getApiEndpoint().getBoards()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(boards -> view.setData(boards), view::onError));
    }

    void sendData() {
        JsonObject params = new JsonObject();
        params.addProperty("name", "some name");

        disposeOnFinish(apiClient.getApiEndpoint().sendBoard(params)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(board -> view.addItem(board), view::onError));
    }

    private void disposeOnFinish(Disposable disposable) {
        compositeDisposable.add(disposable);
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    private void onDestroy() {
        compositeDisposable.dispose();
    }
}
