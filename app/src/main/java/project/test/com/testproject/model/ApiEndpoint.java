package project.test.com.testproject.model;


import com.google.gson.JsonObject;

import java.util.List;

import io.reactivex.Single;
import okhttp3.ResponseBody;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface ApiEndpoint {
    @GET("boards")
    Single<List<Board>> getBoards();

    @POST("boards")
    Single<Board> sendBoard(@Body JsonObject params);
}
