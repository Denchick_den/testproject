package project.test.com.testproject;


import android.app.Application;
import android.content.Context;

import project.test.com.testproject.di.AppComponent;
import project.test.com.testproject.di.DaggerAppComponent;

public class App extends Application {
    private AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        getAppComponent().inject(this);
    }

    public static App getInstance(Context context) {
        return (App) context.getApplicationContext();
    }

    public AppComponent getAppComponent() {
        if (appComponent == null) {
            appComponent = DaggerAppComponent
                    .builder()
                    .context(this)
                    .build();
        }
        return appComponent;
    }
}
