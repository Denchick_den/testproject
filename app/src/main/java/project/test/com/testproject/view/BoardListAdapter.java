package project.test.com.testproject.view;


import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import project.test.com.testproject.R;
import project.test.com.testproject.databinding.ItemBoardBinding;
import project.test.com.testproject.model.Board;

class BoardListAdapter extends RecyclerView.Adapter<BoardListAdapter.ViewHolder> {

    private List<Board> boards = new ArrayList<>();

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_board, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.boardBinding.name.setText(boards.get(position).getName());
        holder.boardBinding.userDelete.setText(boards.get(position).isDelete() ? "delete" : "live");
        holder.boardBinding.number.setText(String.valueOf(position));
    }

    @Override
    public int getItemCount() {
        return boards.size();
    }

    void setBoards(List<Board> boards) {
        this.boards = boards;
        notifyDataSetChanged();
    }

    void setItem(Board board) {
        boards.add(board);
        notifyItemInserted(boards.size() - 1);
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ItemBoardBinding boardBinding;

        ViewHolder(View itemView) {
            super(itemView);
            boardBinding = DataBindingUtil.bind(itemView);
        }
    }
}
