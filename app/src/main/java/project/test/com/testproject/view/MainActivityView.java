package project.test.com.testproject.view;


import java.util.List;

import project.test.com.testproject.model.Board;

interface MainActivityView {
    void setData(List<Board> boards);

    void addItem(Board board);

    void onError(Throwable error);
}
